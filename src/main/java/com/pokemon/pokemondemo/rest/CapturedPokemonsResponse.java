package com.pokemon.pokemondemo.rest;

import com.pokemon.pokemondemo.DB.CapturedPokemon;
import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Getter
@Setter
public class CapturedPokemonsResponse {
List<CapturedPokemon> capturedPokemons;
}
