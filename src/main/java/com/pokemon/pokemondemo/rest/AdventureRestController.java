package com.pokemon.pokemondemo.rest;

import com.pokemon.pokemondemo.DB.PokemonModel;
import com.pokemon.pokemondemo.service.CaptureService;
import com.pokemon.pokemondemo.service.PokemonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.pokemon.pokemondemo.service.EncounterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Endpoint that allows
 * - the user to wander the world to encounter a pokemon
 * - the user to attempt to capture a pokemon
 */
@Slf4j
@RestController
public class AdventureRestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdventureRestController.class);

    @Autowired
    EncounterService encounterService;

    @Autowired
    CaptureService captureService;

    @Autowired
    PokemonService pokemonService;

    /**
     * Wandering endpoint that can trigger an encounter.
     * @return
     */
    @RequestMapping("api/pokemon/encounter")
    @ResponseBody
    public EncounterResponse encounter() {
        LOGGER.info("api/pokemon/encounter");
        return encounterService.encounterPokemon();
    }

    /**
     * Allows the player an attempt to capture the encountered pokemon
     * @param encounter_token
     * @return
     */
    @RequestMapping("api/pokemon/encounter/capture/{encounter_token}")
    @ResponseBody
    public CaptureResponse capture(@PathVariable String encounter_token) {
        return captureService.capturePokemon(encounter_token);
    }

    @RequestMapping("api/pokemon/captured")
    @ResponseBody
    public CapturedPokemonsResponse getCapturedPokemons() {
        return captureService.getCapturedPokemons();
    }


    @RequestMapping("api/pokemon/id/{id}")
    @ResponseBody
    public PokemonModel getPokemonById(@PathVariable long id) {
        return pokemonService.getPokemonById(id);

    }

    @RequestMapping("api/pokemon/name/{name}")
    @ResponseBody
    public PokemonModel getPokemonByName(@PathVariable String name) {
        return pokemonService.getPokemonByName(name);
    }
}



