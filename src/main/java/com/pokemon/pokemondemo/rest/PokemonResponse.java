package com.pokemon.pokemondemo.rest;

import com.pokemon.pokemondemo.DB.PokemonModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PokemonResponse {
    private PokemonModel pokemon;
}
