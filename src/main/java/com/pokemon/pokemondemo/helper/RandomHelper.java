package com.pokemon.pokemondemo.helper;

import java.util.Random;
import java.util.UUID;

public class RandomHelper {
    public static boolean getRandomBoolean() {
        Random r = new Random();
        return r.nextBoolean();
    }

    public static String getShortUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString().substring(0, 7);
    }
}
