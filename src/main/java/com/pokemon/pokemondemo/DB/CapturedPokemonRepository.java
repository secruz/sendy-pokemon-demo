package com.pokemon.pokemondemo.DB;

import org.springframework.data.repository.CrudRepository;

public interface CapturedPokemonRepository extends CrudRepository<CapturedPokemon, Long> {
}


