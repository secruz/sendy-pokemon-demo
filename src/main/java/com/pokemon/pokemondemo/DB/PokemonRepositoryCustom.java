package com.pokemon.pokemondemo.DB;

public interface PokemonRepositoryCustom {

      public PokemonModel findById(long id);

      public PokemonModel findByName(String name);

}
