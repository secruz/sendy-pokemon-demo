package com.pokemon.pokemondemo.DB;


import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;


@Repository
public class PokemonRepositoryCustomImpl implements PokemonRepositoryCustom{

    @PersistenceContext
    private EntityManager em;

    public PokemonModel findById(long id) {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<PokemonModel> cq = cb.createQuery(PokemonModel.class);
        Root<PokemonModel> root = cq.from(PokemonModel.class);
        cq.select(root).where(cb.equal(root.get(PokemonModel_.id), id));

        TypedQuery<PokemonModel> query= em.createQuery(cq);
        return query.getSingleResult();
    }

    public PokemonModel findByName(String name) {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<PokemonModel> cq = cb.createQuery(PokemonModel.class);
        Root<PokemonModel> root = cq.from(PokemonModel.class);
        cq.select(root).where(cb.equal(root.get(PokemonModel_.name), name));

        TypedQuery<PokemonModel> query= em.createQuery(cq);
        return query.getSingleResult();
    }
}
