package com.pokemon.pokemondemo.service;

import com.pokemon.pokemondemo.helper.RandomHelper;
import com.pokemon.pokemondemo.DB.EncounterModel;
import com.pokemon.pokemondemo.DB.EncounterRepository;
import com.pokemon.pokemondemo.DB.PokemonModel;
import com.pokemon.pokemondemo.DB.PokemonRepository;
import com.pokemon.pokemondemo.rest.EncounterResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.concurrent.ThreadLocalRandom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Service
public class EncounterService {
    private static final Logger LOGGER = LoggerFactory.getLogger(EncounterService.class);

    @Autowired
    EncounterRepository encounterRepository;

    @Autowired
    PokemonRepository pokemonRepository;

    public EncounterResponse encounterPokemon() {

        EncounterResponse response = new EncounterResponse();
        boolean random = RandomHelper.getRandomBoolean();
        LOGGER.debug("[ENCOUNTER] should encount pokemon: {}.", random);
        if (random) {
            response.setEncounter(true);
            response.setEncounterToken(RandomHelper.getShortUUID());
            LOGGER.info("[ENCOUNTER] generating a random encounter token: {}", RandomHelper.getShortUUID() );

            Long maxId = pokemonRepository.findMaxId();
            LOGGER.info("Max ID: {}", maxId);
            Long randomPokemonId = ThreadLocalRandom.current().nextLong(1, maxId) + 1;
            PokemonModel pokemon = pokemonRepository.findById(randomPokemonId).get();
            response.setPokemon(pokemon);
            LOGGER.info("Pokemon encountered: {} ", response.getPokemon().getName());
            EncounterModel encounterModel = new EncounterModel();
            encounterModel.setPokemon(pokemon);
            encounterModel.setHash(response.getEncounterToken());
            encounterRepository.save(encounterModel);
        }else{
            LOGGER.warn("Pokemon was not encountered");
        }
        return response;
    }
}
