package com.pokemon.pokemondemo.service;

import com.pokemon.pokemondemo.DB.PokemonModel;
import com.pokemon.pokemondemo.DB.PokemonRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PokemonService {

    @Autowired
    PokemonRepositoryCustom pokemonRepositoryCustom;

    public PokemonModel getPokemonById(long id) {
        return pokemonRepositoryCustom.findById(id);
    }

    public PokemonModel getPokemonByName(String name) {
        return pokemonRepositoryCustom.findByName(name);
    }
}
