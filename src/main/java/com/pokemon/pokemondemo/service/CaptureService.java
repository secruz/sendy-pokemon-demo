package com.pokemon.pokemondemo.service;

import com.pokemon.pokemondemo.DB.PokemonRepository;
import com.pokemon.pokemondemo.DB.EncounterRepository;
import com.pokemon.pokemondemo.DB.CapturedPokemonRepository;
import com.pokemon.pokemondemo.DB.CapturedPokemon;
import com.pokemon.pokemondemo.DB.EncounterModel;
import com.pokemon.pokemondemo.helper.RandomHelper;
import com.pokemon.pokemondemo.rest.CaptureResponse;
import com.pokemon.pokemondemo.rest.CapturedPokemonsResponse;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.server.ResponseStatusException;
import java.util.List;

@Service
public class CaptureService {

    @Autowired
    PokemonRepository pokemonRepository;

    @Autowired
    EncounterRepository encounterRepository;

    @Autowired
    CapturedPokemonRepository capturedPokemonRepository;

    public CaptureResponse capturePokemon(@PathVariable String encounter_token) {
        EncounterModel encounter = encounterRepository.findByHash(encounter_token);
        if(encounter != null) {
            CaptureResponse captureResponse = new CaptureResponse();
            int attempts = encounter.getCaptureAttempts();
            int totalAttempts = attempts + 1;
            if (attempts < 3) {
                if(RandomHelper.getRandomBoolean()) {
                    CapturedPokemon capturedPokemon = new CapturedPokemon();
                    capturedPokemon.setPokemon(encounter.getPokemon());
                    capturedPokemonRepository.save(capturedPokemon);
                    encounterRepository.delete(encounter);
                    captureResponse.setRemainingAttempts(0);
                    captureResponse.setCaptured(true);
                    captureResponse.setPokemon(encounter.getPokemon());
                } else {
                    encounter.setCaptureAttempts(totalAttempts);
                    encounterRepository.save(encounter);
                    captureResponse.setRemainingAttempts(3 - totalAttempts);
                }
            } else {
                encounterRepository.delete(encounter);
                throw new ResponseStatusException(
                        HttpStatus.NOT_FOUND, "Encounter not found"
                );
            }

            return captureResponse;
        }
        throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "Encounter not found"
        );
    }

    public CapturedPokemonsResponse getCapturedPokemons() {
        CapturedPokemonsResponse capturedResponse = new CapturedPokemonsResponse();
        List<CapturedPokemon> response = IterableUtils.toList( capturedPokemonRepository.findAll());
        capturedResponse.setCapturedPokemons(response);

        return capturedResponse;
    }
}