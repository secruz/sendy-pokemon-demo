package com.pokemon.pokemondemo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class PokemonDemoApplication {
	public static void main(String[] args) {
		SpringApplication.run(PokemonDemoApplication.class, args);
	}

}
