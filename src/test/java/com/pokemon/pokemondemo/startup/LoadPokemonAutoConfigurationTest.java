package com.pokemon.pokemondemo.startup;

import com.pokemon.pokemondemo.DB.PokemonModel;
import com.pokemon.pokemondemo.DB.PokemonRepository;
import org.junit.jupiter.api.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class LoadPokemonAutoConfigurationTest {
    @Test
    public void test() {
        PokemonRepository pokemonRepository = mock(PokemonRepository.class);
        LoadPokemonConfiguration loadPokemonAutoConfiguration = new LoadPokemonConfiguration(pokemonRepository, 10);
        loadPokemonAutoConfiguration.startup();
        verify(pokemonRepository, times(9)).save(any(PokemonModel.class));
    }

}